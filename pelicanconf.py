#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Sebastian Garcia'
AUTHORS = u'Sebastian Garcia'
SITENAME = u'Stratosphere IPS'
SITEURL = 'https://stratosphereips.org'
SITEURL = ''
TWITTER_USERNAME = 'stratosphereips'
GITHUB_URL = 'https://github.com/stratosphereips'
PDF_GENERATOR = True

PATH = 'content'

STATIC_PATH= ['images']

TIMEZONE = 'Europe/Bratislava'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

WITH_FUTURE_DATES = True
#WITH_FUTURE_DATES = False

#
# Nice and simple theme
#THEME = '../pelican-themes/blueidea'
#THEME = '../pelican-themes/bootstrap'
#THEME = '../pelican-themes/bootstrap2'
#THEME = '../pelican-themes/cebong'

# Very simple, white. Miss the links.
#THEME = '../pelican-themes/chamaleon'
#THEME = '../pelican-themes/built-texts/'
# chunk. Nice, strong, big font. Problem with the menu.
#../pelican-themes/new-bootstrap2/

# Bests
#THEME = '../pelican-themes/elegant'
#THEME = '../pelican-themes/pelican-bootstrap3/' 
#THEME = '../pelican-themes/Responsive-Pelican/'

#Selected
THEME = 'themes/foundation-default-colours'

SITELOGO = 'images/stratosphereips-logo-2.jpg'

INDEX_SAVE_AS = 'blog_index.html'

TYPOGRIFY = True

# Blogroll
LINKS = (('NlNet', 'http://nlnet.nl/'),
         ('CVUT', 'http://agents.felk.cvut.cz/'),
         ('MCFP', 'http://mcfp.felk.cvut.cz/'),
        )

# Social widget
SOCIAL = (('Stratosphere GitHub', 'https://github.com/stratosphereips'),
         ('Stratosphere Twitter', 'https://twitter.com/StratosphereIPS'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

GOOGLE_ANALYTICS = 'UA-59793876-1'
#FOUNDATION_PYGMENT_THEME = 'friendly'
FOUNDATION_PYGMENT_THEME = 'tango'
FOUNDATION_FRONT_PAGE_FULL_ARTICLES = False


