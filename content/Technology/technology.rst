Technology in the Stratosphere IPS Project
##########################################
:date: 2015-01-21
:tags: technology
:save_as: category/technology.html



The core of the Stratosphere IPS is composed of what we called *network behavioral models* and detection algorithms. The behavioral models represent what a specific connection does in the network during its life time. The behavior is constructed by analyzing the periodicity, size and duration of each flow. Based on these features each flow is assigned a letter and the group of letters characterize the behavior of the connection. 

For example, the connection identified with the 4-tuple **10.0.2.106-212.124.126.66-80-tcp**, that was assigned the label *From-Botnet-V1-TCP-CC102-HTTP-Custom-Encryption* had the following behavioral model: 

.. pull-quote::

    11aaaaaaaaaaabrrctrraaaAaaaaaAaaaaaaaaaaaaaaaaaAAAaaaaaaaaaaaaaaaaaaaAaAaaa
    aaaaaaaaaaaaaaaaaaaaAAaaaaaaaaAAaaaaaaaaaaaaaaaaaaaaaaaaAAAAaaaaaaaaAAaaAAa
    aaaaaaaaaaaaaaaaaaaaAAaaaaaaaaaAAaaaaaaaaaaaaaaaaaaaaaaaaaaaaAAaAaaaaaaaaaa
    aaaaaaaaaaaaaaaaaaaaaaaaAaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaAAaaaaaaaaaaa(...)

This chain of states that we call the behavioral model highlight some of the characteristics of the C&C channel. In this case it tell us that that flows are highly periodic (letters 'a', 'b', 'c', and 'A'), with some lost periodicity near the beginning (letters 'r' and 't'). The flows were also rather small and short. Using this models we are able to generate the behavioral characteristics of a large number of malicious actions.

Our detection algorithms use our known malicious behavioral models to detect new suspicious connections in the network. The detection is currently done using Markov Chains-based algorithms. Please see the `Publications section`_ for technical details about the algorithms.

The first part of our algorithm consist in learning and labeling the ground-truth traffic. This traffic is used to create verified models of network behaviors that are known and stable. Not all the models seen in the malicious traffic are used, since the models should have certain properties. For example, each model has a specific *detection performance* that has to be evaluated before putting it on production. 

The second part of the algorithm consists in using these *known* and *verified* ground-truth models to detect similar behaviors in unknown networks. The Stratosphere IPS will capture traffic in a client computer and it will compare each unknown connection to the known models of traffic behavior. Because how the detection is done and how the models are created, each behavioral model can match a wide range of similar behaviors without being *too* general. The models are then useful to find similar behaviors without the risk of generating too much False Positives.

The Stratosphere IPS is not strictly an IPS in the sense that it can not *prevent* the intrusion. We use the IPS acronym because the Stratosphere IPS can block malicious connections using the firewall in your computer. However, due to the nature of the traffic connections, the Stratosphere IPS needs some time to detect the malicious behavior and hence can not block the first packets in the connection. The Stratosphere IPS can detect and block very subtle and dangerous network connections, and so it should be seen as a complement of current network security measures. 

It Complements the Current Protection Mechanisms
------------------------------------------------
The Stratosphere IPS does not analyze individual files or URL, nor it uses individual IP addresses or domains. Therefore, it is not a replacement for current protection technologies, such as IDS or Antivirus. The Stratosphere IPS works by detecting and blocking  It will block all the connections that are known to behave maliciously, such as the Command and Controls channels of botnets.

Continuous Verification
-----------------------
The development of a detection algorithm is strongly related with the capacity to measure its performance. This implies that some results can be achieved, and that the performance measurement is able to help in the selection of the best algorithm by making some comparisons. However, to obtain some results it is necessary to have good datasets, and this is the key of the problem. The performance metrics and the definition of what a good algorithm is depends on the datasets used. Even if we think about trying the algorithm in a real network, this network can be seen as a special type of dataset.  This is why it is so important in the Stratosphere IPS to have a continuous verification of the algorithms in real datasets, giving us an adapting idea of how good our detections are. The datasets include malware, normal and background types of traffic. This continuous verification is done in the `Stratosphere Testing Framework`_.

Funding
-------
The Stratosphere IPS was started in the `CTU University`_, in Prague, Czech Republic and is currently being funded by the `NLnet Foundation`_ from Netherlands. We thank the NLnet foundation for their support, trust and commitment to an open and free Internet.


.. _`Stratosphere Testing Framework`: {filename}../Development/development-stratosphere-testing-framework.md
.. _`CTU University`: http://www.felk.cvut.cz
.. _`NLnet Foundation`: http://www.nlnet.nl
.. _`Publications section`: publications.html

