Title: Stratosphere IPS. Generation of the Behavioral Models
Date: 2015-10-15
Category: Research Blog

The Stratosphere Project aims at detecting the behavioral patterns of malicious connections in unknown networks by modelling the behaviors of __known__ malicious traffic. To accomplish this, it applies several different machine learning methods for detecting the behaviors. This blog post is a theoretical description of how the behaviors are represented and interpreted before any detection algorithm is implemented. We call this the __disassociation__ between the _representation of the model_ and the _detection_ of the model. 

## Representation of the Patterns in the Network: An Update on the String of Letters
Stratosphere is based on the analysis of network traffic in the form of flows. The flow standard used is the [Argus tool](qosient.com/argus/). Argus is used to read the packets from a pcap file or network and to generate the flows. After the flows are generated, Stratosphere groups them according to a very simple logic: Group together the flows that share:

- The same source IP address.
- The same destination IP address.
- The same destination port.
- The same protocol.


The flows, then end up being grouped in what we called __4-tuples connections__ or just __connections__. The motivation is that a connection has all the information of a client interacting with a specific _service_. For example if you connect to a Facebook, you will send from time to time packets to a port and destination IP. These packets carry your behavior when connecting with this web server. If you click on links, read messages, chat or post, this will become your behavior in using that web page. In the same way malware connects to a specific server (such as C&C server) and shows specific behaviors when doing it.

So a connection has many flows in it and represents a specific behavior. Stratosphere analyzes these flows to create a good representation of the patterns in the network. The idea is that each new flow updates the state of the connection and therefore we can assign a different state to each flow. To assign a state is necessary to extract several features from the flow. The features extracted are four:

- The size of the flow.
- The duration of the flow.
- The periodicity of the flow.
- The time of the flow.

The size, duration and time are simple to extract because the information is in the same flow. The periodicity is a little more complex and needs a separate explanation.

### Computing the Periodicity of a Flow
There have been several approaches to compute the periodicity of network traffic before. However, our approach is radically different because of three reasons. First, we use flows and not packets. Second, since we know the periodicity of malware connection changes all the time, we need to update the periodicity on each flow. Third, the periodicity has a certain variance, and therefore we should account for it. Therefore, every time a new flow arrives, its periodicity is computed as follows:

- Take the time stamp of the current flow. Lets call it _a_.
- Take the time stamp of the previous flow. Lets call it _b_.
- Take the time stamp of two flows ago. Lets call it _c_.

Then compute
    
> T1 = a - b

and

> T2 = b - c

and finally

> TD = |T2 - T1|

TD is the value we will use as periodicity.

### Computing the State of a Flow as a Letter
Now that the size, duration, periodicity and time of the flow were computed, Stratosphere uses them to generate a state for the flow. This is done by a discretization of the values using some thresholds. The thresholds used are:

- First size threshold: 250
- Second size threshold: 1100
- First duration threshold: 0.1
- Second duration threshold: 10
- First periodicity threshold: 1.05
- Second periodicity threshold: 1.3
- Third periodicity threshold: 5
- Timeout threshold: 3,600

The threshold are trained in a special way that is outside the scope of this blog. Using the threshold we can now assign a letter using the following assignment logic:

![letter assignment strategy]({filename}../images/letter-assignment-behavioral-models.png) 

So each threshold divides the feature space. For example, if a flow has size=200, duration=2 and TD=1.20 Stratosphere will consider its size as _small_, its duration as _medium_ and its periodicity _Weak Periodicty_. After selecting the letter, the time of the flow is used to select the symbol for the time difference. The symbols represent how much separated the flows are and are useful because a periodicity of 1 second is _not_ the same as a periodicity of _1 day_.

Similarly and using this table we can assign a letter to each flow in the connection. For example, if we look at the malware capture [CTU-Malware-Capture-Botnet-116-4](https://mcfp.felk.cvut.cz/publicDatasets/CTU-Malware-Capture-Botnet-116-4/) from the Stratosphere Dataset, we can find a 4-tuple connection identified as _192.168.0.150-46.105.227.94-80-tcp_. This connection represents a C&C channel sending HTTP requests such as

    POST /cmd.php HTTP/1.1
    Connection: Keep-Alive
    Content-Type: text/plain; Charset=UTF-8
    Accept: */*
    User-Agent: Mozilla/4.0 (compatible; Win32; WinHttp.WinHttpRequest.5)
    Content-Length: 0
    Host: 46.105.227.94

When analyzed with Stratosphere the flows in this connection generate the following strings of letters:

>   88+H+y+H+H+h+H+H+H+y+y+y+y+y+H+H+y+y+H+y+h+y+y+H+y+y+y+H+h+y+h+H+H+y+

>   h+y+H+y+H+H+y+y+y+H+I+h+y+y+y+y+y+h+y+y+y+H+H+y+H+y+y+y+y+y+H+H+H+y+

>   y+y+y+y+y+y+y+y+h+h+y+h+y+y+y+h+H+H+H+H+H+H+y+H+y+h+y+h+y+h+H+y+y+H+


The letters in the string shows that this connection is highly  __periodict__, with some strong periodic letters _h_ and some weak periodic letters _H_. From time to time it can be seen that the periodicity is lost when the letter _y_ is used. The symbol _+_ is very important and it means that the freq of the flows is between 1 minute and 5 minutes.


This is how Stratosphere _represents_ the behavior of connections in the network. This representation is independent of any detection method and is very useful for visualizing the behaviors, for verifying the behaviors and for applying any detection method on top of it.


## The Behavioral Models: Interpreting the String of Letters as a Markov Chain 
Stratosphere uses this representation of the behavior as a basis for its detection methods. The first detection method that Stratosphere implemented and that is already published in the Stratosphere Testing Framework is the __first order Markov Chain Model__.

The first order Markov Chain model (or 1MC for short) is the interpretation of these string of letters as a Markov Chain. That is, is a model of the transition probabilities from one state letter to the next. This blog post explains how the 1MC is used to create the behavioral model of a connection. These behavioral models are the core part of Stratosphere, since they are the models that can be shared and that are used for detecting similar traffic later.

The methodology to create a behavioral model fro _one_ connection in Stratosphere is like this:

1. Obtain the string of letters that represents the patterns of the connection.
2. Create a Markov Chain based on those letters. Store both the matrix and the initialization vector.
3. Compute the probability of generating the _original_ string of letters with its own Markov Chain, and store this probability.

For example if we have the letters:

> a,a,c+d+d+

we can generate the following example Markov Chain and matrix:

<center>
<img src="../images/example-mc.png" width="250" height="250" />
<img src="../images/table1.png" width="200" height="200" />
</center>


The behavioral model for this connection, then is the group of values of the _Markov Matrix_, the _Initialization Vector_ and the _probability of detecting itself_.

## Implementation of the String of Letters in the Stratosphere Testing Framework
The good news is that this behavioral models are __already__ implemented in the Stratosphere Testing Framework (STF) and can be used by anyone. Lets see an example implementation step by step, starting from a pcap capture we have. For this example we will use the capture [CTU-Malware-Capture-Botnet-124-1](https://mcfp.felk.cvut.cz/publicDatasets/CTU-Malware-Capture-Botnet-124-1/) from the Stratosphere Dataset. This capture corresponds to a Geodo malware and that you can download and use for yourself.

0. Access STF

    ./stf.py 

        Stratosphere Testing Framework
        https://stratosphereips.org
                 _    __ 
                | |  / _|
             ___| |_| |_ 
            / __| __|  _|
            \__ \ |_| |  
        ... |___/\__|_|  ...
        0.1.5alpha

        
    stf >

1. Loading the pcap file into STF


        stf > datasets -c CTU-Malware-Capture-Botnet-124-1/2015-04-30_capture-win5.pcap
        The name of the dataset or 'Enter' to use the name of the last folder:
        [*] Added file 2015-04-30_capture-win5.pcap to dataset CTU-Malware-Capture-Botnet-124-1
        [*] Dataset CTU-Malware-Capture-Botnet-124-1 added with id 83.


2. Generate the bidirectional Argus files (both binary and flow text)


        CTU-Malware-Capture-Botnet-124-1: stf > datasets -g
        [*] Generating the biargus file.
        [*] Added file 2015-04-30_capture-win5.biargus to dataset CTU-Malware-Capture-Botnet-124-1
        [*] Generating the binetflow file.
        [*] Added file 2015-04-30_capture-win5.binetflow to dataset CTU-Malware-Capture-Botnet-124-1


3. Generate the connections for this dataset

        CTU-Malware-Capture-Botnet-124-1: stf > connections -g


4. Verify that the connections were created


        CTU-Malware-Capture-Botnet-124-1: stf > connections -l
        [*] Groups of Connections Available:
        +----------------------------+------------+-----------------------------------+-----------------------+
        | Id of Group of Connections | Dataset Id | Filename                          | Amount of Connections |
        +----------------------------+------------+-----------------------------------+-----------------------+
        | 83                         | 83         | 2015-04-30_capture-win5.binetflow | 32                    |
        +----------------------------+------------+-----------------------------------+-----------------------+



5. List some connections

        CTU-Malware-Capture-Botnet-124-1: stf > connections -L 83
        | Connection Id | Amount of flows |
        0.0.0.0-10.0.2.105--arp                  | 1
        00:00:00:00:00:00-00:00:00:00:00:00--llc | 1
        10.0.2.105-10.0.2.2--arp                 | 1
        10.0.2.105-10.0.2.255-137-udp            | 1
        10.0.2.105-194.169.227.18-465-tcp        | 1
        10.0.2.105-194.25.134.51-465-tcp         | 1
        10.0.2.105-195.200.35.195-587-tcp        | 10
        10.0.2.105-201.175.12.142-587-tcp        | 10
        10.0.2.105-201.175.17.35-8080-tcp        | 11
        10.0.2.105-212.227.15.167-25-tcp         | 6
        10.0.2.105-212.227.15.183-25-tcp         | 4
        10.0.2.105-212.227.15.183-465-tcp        | 1
        10.0.2.105-212.227.17.168-25-tcp         | 10
        10.0.2.105-212.227.17.168-587-tcp        | 20
        10.0.2.105-212.227.17.190-587-tcp        | 4
        10.0.2.105-212.247.156.7-465-tcp         | 1
        10.0.2.105-213.165.67.108-25-tcp         | 1
        10.0.2.105-213.165.67.124-25-tcp         | 9
        10.0.2.105-213.46.255.2-587-tcp          | 10
        10.0.2.105-217.31.82.111-25-tcp          | 10
        10.0.2.105-224.0.0.252-5355-udp          | 1
        10.0.2.105-8.8.4.4-53-udp                | 2
        10.0.2.105-8.8.8.8-53-udp                | 109
        10.0.2.105-81.169.145.133-25-tcp         | 10
        10.0.2.2-10.0.2.105--arp                 | 1
        10.0.2.2-10.0.2.105--icmp                | 1
        10.0.2.2-10.0.2.105-0x0a00-icmp          | 1
        ::-ff02::1:ffce:e387--ipv6-icmp          | 1
        fe80::d5e6:502a:54ce:e387-ff02::16--ipv6-icmp | 1
        fe80::d5e6:502a:54ce:e387-ff02::1:2-547-udp | 20
        fe80::d5e6:502a:54ce:e387-ff02::1:3-5355-udp | 1
        fe80::d5e6:502a:54ce:e387-ff02::2--ipv6-icmp | 1
        Amount of connections printed: 32


6. Generate the Behavioral Models with the Strings of letters


        CTU-Malware-Capture-Botnet-124-1: stf > models -g

7. See some of the connections with its string of letters. Filter some connections so we focus on the important ones (leave outside ARP, ICMP and LLC)


        CTU-Malware-Capture-Botnet-124-1: stf > models -L 83-1 -a 100 -f name!=arp name!=llc name!=fe name!=icmp
         Note | Label | Model Id | State |
        [   ] |  | 10.0.2.105-10.0.2.255-137-udp    | 5
        [   ] |  | 10.0.2.105-194.169.227.18-465-tcp| 5
        [   ] |  | 10.0.2.105-194.25.134.51-465-tcp | 5
        [   ] |  | 10.0.2.105-195.200.35.195-587-tcp| 88.y.y.y,y.y,y.H.H,
        [   ] |  | 10.0.2.105-201.175.12.142-587-tcp| 88.y.H.y.y,y.y.y,y.
        [   ] |  | 10.0.2.105-201.175.17.35-8080-tcp| 99,i,I,i,I,i,z+Z,I,i,
        [   ] |  | 10.0.2.105-212.227.15.167-25-tcp | 88,y.H.y,y.
        [   ] |  | 10.0.2.105-212.227.15.183-25-tcp | 88,H,s,
        [   ] |  | 10.0.2.105-212.227.15.183-465-tcp| 5
        [   ] |  | 10.0.2.105-212.227.17.168-25-tcp | 88.y.y.H.y.y.h.h.y.
        [   ] |  | 10.0.2.105-212.227.17.168-587-tcp| 88.y,y.y.y.Y*Y,y.y.H.y,y.h.h.H.Y+Y.y.s.
        [   ] |  | 10.0.2.105-212.227.17.190-587-tcp| 88.y.Y,
        [   ] |  | 10.0.2.105-212.247.156.7-465-tcp | 5
        [   ] |  | 10.0.2.105-213.165.67.108-25-tcp | 8
        [   ] |  | 10.0.2.105-213.165.67.124-25-tcp | 88.y,y.H.h.h.y,y.
        [   ] |  | 10.0.2.105-213.46.255.2-587-tcp  | 88.y.Y,H,y.H.y,H,H,
        [   ] |  | 10.0.2.105-217.31.82.111-25-tcp  | 88.H.z.Y,Z,z,z,z,z,
        [   ] |  | 10.0.2.105-224.0.0.252-5355-udp  | 1
        [   ] |  | 10.0.2.105-8.8.4.4-53-udp        | 11.
        [   ] |  | 10.0.2.105-8.8.8.8-53-udp        | 11,R.r.r,r.r.a.r.r.r.s.r.r.A.r.s,r.r.r,r.r.r.R.r.A.r.r.A.r,A,r,r.r.R,A,r.A.r,A,A,r.R.R.r.r.r,r.r,r.A
        [   ] |  | 10.0.2.105-81.169.145.133-25-tcp | 88.H.y.h.y,H,H,h,y.



8. Select some connection to analyze the flows. 


For example we can analyze the connection __10.0.2.105-201.175.17.35-8080-tcp__ which seems interesting given that it has periodic letters __i__ and __I__ with frequency between 5 seconds and 1 minute. In the following output we trimmed the content of the flow for the blog. Typically you should see more data inside the flow. Please scroll right to see the POST command.


        CTU-Malware-Capture-Botnet-124-1: stf > connections -F 10.0.2.105-201.175.17.35-8080-tcp
        StartTime,Dur,Proto,SrcAddr,Sport,Dir,DstAddr,Dport,State,sTos,dTos,TotPkts,TotBytes,SrcBytes,srcUdata,dstUdata,Label
         State: "9" TD: -1.0 T2: False T1: False        1970/01/01 01:01:05.307410,37.788639,tcp,10.0.2.105,   ->,201.175.17.35,8080,FSPA_FSPA,0,0,21,8312,1004,s[640]=POST /spoMmsue93kSmh/naSjieU83llsv2Q/ HTTP/1.1
         State: "9," TD: -1.0 T2: 0:00:37.789151 T1: False      1970/01/01 01:01:43.096561,37.042778,tcp,10.0.2.105,   ->,201.175.17.35,8080,FSPA_FSPA,0,0,15,5968,683,s[392]=POST /spoMmsue93kSmh/naSjieU83llsv2Q/ HTTP/1.1
         State: "i," TD: 1.020139 T2: 0:00:37.043140 T1: 0:00:37.789151 1970/01/01 01:02:20.139701,43.535404,tcp,10.0.2.105,   ->,201.175.17.35,8080,FSPA_FSPA,0,0,15,5353,683,s[392]=POST /spoMmsue93kSmh/naSjieU83llsv2Q/ HTTP/1.1
         State: "I," TD: 1.175268 T2: 0:00:43.535605 T1: 0:00:37.043140 1970/01/01 01:03:03.675306,42.187801,tcp,10.0.2.105,   ->,201.175.17.35,8080,FSPA_FSPA,0,0,16,5990,683,s[392]=POST /spoMmsue93kSmh/naSjieU83llsv2Q/ HTTP/1.1


The most interesting part of this output is the confirmation of the frequency, which can be seen in the value of T2 or T1. It seems to be around 37 seconds.


9. Create and assign a label for the connection.

The process of creating a label is very simple. Once you know what the connection is you simply select from a menu the characteristics of the labels.

        CTU-Malware-Capture-Botnet-124-1: stf > labels -a -c 10.0.2.105-201.175.17.35-8080-tcp -g 83-1

The -a parameter means to add a label, -c specifies the connection and -g is the id of the groups of models in the dataset.


        CTU-Malware-Capture-Botnet-124-1: stf > labels -a -c 10.0.2.105-201.175.17.35-8080-tcp -g 83-1
        [!] Remember that a label should represent a unique behavioral model!
        Select a label Id to assign the same label BUT with a new final number to the current connection. Or press Enter to create a new one:
        Please provide a direction. It means 'From' or 'To' the most important IP in the connection: 
        From
        Please provide the main decision. 'Botnet', 'Malware', 'Normal', 'Attack', or 'Background': 
        Botnet
        Please provide the layer 3 proto. 'TCP', 'UDP', 'ICMP', 'IGMP', or 'ARP': 
        TCP
        Please provide the main proto in layer 4. 'HTTP', 'HTTPS', 'FTP', 'SSH', 'DNS', 'SMTP', 'P2P', 'NTP', 'Multicast', 'NetBIOS', 'Unknown', 'Other', 'Custom', or 'None': 
        HTTP
        Please provide optional details for this connection. Up to 30 chars (No - or spaces allowed). Example: 'Encrypted', 'PlainText', 'CustomEncryption', 'soundcound.com', 'microsoft.com', 'netbios': 
        CC
        [*] Connection has note id 7195


You can now see the labels created 

        CTU-Malware-Capture-Botnet-124-1: stf > labels -l
        | 1 | From-Botnet-TCP-HTTP-CC-28 | 83-1 | ['10.0.2.105-201.175.17.35-8080-tcp']


10. The last step is to create the Markov Chain model for this specific label.

        CTU-Malware-Capture-Botnet-124-1: stf > markov_models_1 -g From-Botnet-TCP-HTTP-CC-28

And this can be confirmed listing the markov models

         1 | 21 | 1 | From-Botnet-TCP-HTTP-CC-28  | False |  -1 | 99,i,I,i,I,i,z+Z,I,i,




## Conclusion

In this blog post we summarized the theory behind the creation of the behavioral models in Stratosphere. This was clarified with a complete example of the creation of a Markov model inside Stratosphere Testing Framework with a real and published capture. We show how to import the dataset, how to create the 4-tuples connections, how to label a connection and how to create the Markov Chain model. With this data, you are ready to start analyzing the behaviors in your own captures. In a future blog post we will describe how Stratosphere is using these Markov models to detect similar behaviors in unknown networks.




