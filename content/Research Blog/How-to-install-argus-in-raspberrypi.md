Title: How to install and run Argus sniffer in your Raspberry Pi
Date: Mon Mar  12 2015
Category: Research Blog

This is an install guide to run the [Argus Sniffer](https://qosient.com/argus/) in the Raspberry PI using Raspbian for use in the Stratosphere Project.

## Install Argus
1. Download Latest Argus 

    > wget http://qosient.com/argus/dev/argus-latest.tar.gz


2. Install dependencies
    
    > sudo apt-get update
    >
    > sudo apt-get install bison flex libpcap-dev libpcap0.8 daemontools

3. Unpack

    > tar xfz argus-latest.tar.gz

4. Compile Argus server

    >    cd argus-*
    >
    >    ./configure
    >
    >    make
    >
    >    sudo make install

## Install Stratosphere Argus configuration

The instructions and files needed to install the Stratosphere configuration are in https://github.com/stratosphereips/argus-configuration-files.git. The steps are:

1. git clone https://github.com/stratosphereips/argus-configuration-files.git
2. cd argus-configuration-files
3. ./install.sh

This configuration will also run argus with supervise (so it is restarted when it dies).

# How does everything work?
Argus is a sniffer that will capture packets in your Rpi and send flows to the Stratosphere project for analysis and protection. Once installed argus will run your Rpi and be restarted if it dies.
