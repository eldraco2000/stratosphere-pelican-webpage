Close Source Algorithms, Reproducibility and Influence in the Attacker Community
################################################################################
:date: 2014-01-22
:tags: Machine Learning
:status: draft

About the problem of close-source algorithms, irreproducibility. This is good for avoiding the
attackers to know how to evade the IPS. This is bad for the testing and confidence of the community
and therefore its advancement.
