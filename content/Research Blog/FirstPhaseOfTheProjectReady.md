Title: First Phase of the Stratosphere IPS Project Ready
Date: Mar  24 2015
Category: Research Blog

The Stratosphere IPS Project officially started around January 2015 with a huge effort of development and planning. We are glad to see that after these two months we were able to start the project website and social relationships, to boost the collaboration with others and to develop the first part of the project: The Stratosphere Testing Framework. 

With the framework available for testing, the captures being done and the experiments running, we are pushing forward the idea of detecting network traffic by identifying the behaviors in the network. And we are working on more ideas! Thanks NLnet for your great support in this project.


