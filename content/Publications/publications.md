Title: Publications 
Date: 2015-01-20 05:40 
Tags: Publications
save_as: category/publications.html


These are the current research publications related to the Stratosphere IPS project.

# Journals and Books
- Garcia, S., Grill, M., Stiborek, J., & Zunino, A. (2014). An Empirical Comparison of Botnet Detection Methods. Computers & Security, 45(0), 100 – 123. doi (http://dx.doi.org/10.1016/j.cose.2014.05.011) [Download](http://www.sciencedirect.com/science/article/pii/S0167404814000923)
- Garcia, S., Zunino, A., & Campo, M. (2013). Survey on Network-based Botnet Detection Methods. Security and Communication Networks, John Wiley & Sons, Ltd, 7(5), 878–903. doi:10.1002/sec.800 [Download](https://www.researchgate.net/publication/237010147_Survey_on_Network-based_Botnet_Detection_Methods)
- Garcia, S. (2014). Identifying, Modeling and Detecting Botnet Behaviors in the Network. UNICEN University. PhD Thesis. doi:(10.13140/2.1.3488.8006), [Download](https://www.researchgate.net/publication/271204142_Identifying_Modeling_and_Detecting_Botnet_Behaviors_in_the_Network)

# Conferences
- Garcia, S. (2015). Modelling the Network Behaviour of Malware To Block Malicious Patterns . The Stratosphere Project : A Behavioural IPS. Virus Bulletin Conference. doi: 10.13140/RG.2.1.3784.7765 [Download](https://www.virusbtn.com/pdf/conference_slides/2015/Garcia-VB2015.pdf)
- Garcia, S. (2015). The Network Behavior of Targeted Attacks . Models for Malware Identification and Detection. Hacktivity Conference. doi:10.13140/RG.2.1.2867.2723 [Download](https://hacktivity.com/en/archives/hacktivity-20151/)
