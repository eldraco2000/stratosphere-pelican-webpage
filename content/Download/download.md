Title: Download
date: 2015-01-22
tags: download
save_as: category/download.html       

The Stratosphere Linux IPS, the Stratosphere Windows IPS and the Stratosphere Testing Framework are currently available for download and testing. 


# Descriptions
You can read more about the programs on their current webpages:

-  [Stratosphere Linux IPS](stratospherelinuxips.html "Stratosphere Linux IPS")
-  [Stratosphere Windws IPS](stratospherewindowsips.html "Stratosphere Windows IPS")
-  [Stratosphere STF](https://stratosphereips.org/development-of-the-stratosphere-testing-framework.html "Stratosphere STF")


# Download
To download the code you can visit the git repositories:

- [Code of Stratosphere Testing Framework](https://github.com/stratosphereips/StratosphereTestingFramework)
- [Code of Stratosphere Linux IPS](https://github.com/stratosphereips/StratosphereLinuxIps)
- [Code of Stratosphere Windows IPS](https://github.com/stratosphereips/StratosphereWindowsIps)

Being all of them free software we encourage you to help us with the development of the product and the testing. Bugs reports are welcome!

The malware captures used by the Stratosphere IPS can currently be downloaded from our [Stratosphere Dataset]. There is a huge amount of botnet datasets for you to download and use.

# Malware Capture Facility Project
The Stratosphere Project is now the umbrella project for the [Malware Capture Facility Project]. This means that the MCFP will continue working, producing and making public real datasets of botnets, but the Stratosphere project will be the main destination of those datasets. For every practical reason, the MCFP is now the dataset generation part of the Stratosphere Project.

[Malware Capture Facility Project]: http://mcfp.felk.cvut.cz
[Stratosphere Dataset]: https://stratosphereips.org/category/dataset.html

