Title: SDA
Date: Apr 09 2015
Category: SDA
Tags: cloud, NGOs, detections, machine learning
save_as: category/sda.html

# Protecting NGOs with Stratosphere IPS for Free

## NGOs are at Risk
NGOs are a critical asset for our society, protecting human rights and civil liberties. Their work made them a highly valuable political target for many powerful actors; resulting in continual technical abuses, especially in the use of new technologies. Furthermore, NGOs usually don't know how to best protect themselves, because they lack the knowledge or the resources. This situation is further difficulted because NGOs value their privacy. The CTU University took a step forward to help them.

# SDA. Stratosphere Data Analysis Project
To protect NGOs, the CTU University implemented its Stratosphere Data Analysis Project. The goal of this project is to provide a cloud-based service where we run our machine learning algorithms to remotely detect the malicious behaviors of infected computers in the NGO's network. The goal of SDA is to provide an online service in the CTU University that can receive the flows from organizations and apply the most advanced detection algorithms using Stratosphere. The organizations should only install a small software on their networks to send the flows to SDA. These flows only contain metadata and are highly protected by the University by signing an NDA.

The SDA service is offered to provide the following benefits:

- Organizations can be protected by an advanced detection algorithm for free.
- Organizations do not have to install more than the software that sends the flows.
- Organizations do not have to update, maintain, verify or configure the software.
- The flows are protected by an NDA signed by the University and strong security practices.
- Organizations can be protected as soon as SDA receives the flows.
- The SDA service can be scaled to a large amount of organizations.
- The SDA service can be improved with time, learning from the detections on all the organizations.


