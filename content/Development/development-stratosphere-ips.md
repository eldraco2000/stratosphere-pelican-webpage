Title: Development of the Stratosphere IPS
date: 2015-01-21
tags: development

The development of the Stratosphere IPS is divided in two parts. The first one is the Stratosphere IPS for Windows and the other the Stratosphere IPS for Linux. Although both will do the same detection their development is separated.

# Stratosphere Linux IPS 
The Stratosphere Linux IPS (slips) is currently published as an alpha version for testing by the community. If you want to download this version and test the detection in your computer or network, please see [the slips git repository](https://github.com/stratosphereips/StratosphereLinuxIPS).

# Stratosphere Windows IPS 
The Stratosphere Windows IPS (swips) is currently published as an alpha version for testing by the community. If you want to download this version and test the detection in your computer or network, please see [the swips git repository](https://github.com/stratosphereips/StratosphereWindowsIPS).

The final features of both Stratosphere IPS will include:
- Manual update and download of the behavioral models from the Stratosphere server (already available on the Stratosphere for Linux).
- Detection of malicious behaviors in the network using machine learning (already available on the Stratosphere for Linux).
- Optional blocking of the malicious connection using the clients firewall.
- Visualization of the behavior of the connections so the user can understand what is happening, and manage the different parts of the system (only available in the Stratosphere Testing Framework).
- Strong protection of the privacy of the user. No information will be automatically sent from the client. The client can decide to manually send feedback.


# Current State
The current state of the Stratosphere IPS projects are:

- Stratosphere Linux IPS: Alpha version 0.2 first published on 2015-12-08
- Stratosphere Windows IPS: Alpha version 0.1 first published 2016-05-20

