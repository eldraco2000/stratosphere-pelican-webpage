Title: 
URL:
save_as: index.html

![cosmos](images/kosmos-3.jpg)

# Stratosphere IPS Project

> NEWS!: Stratosphere partened with Cisco systems to implement the Stratosphere detection as a plugin in the future Snort IDS! Check out our git repository with the C version of Stratosphere developed by Cisco. It is [here on GitHub](https://github.com/stratosphereips/StratosphereLibSlips)

> NEWS!: We are now publishing a new type of capture that is Normal traffic mixed with a real infection. The idea is to have a real human doing real actions, then infecting the computer while the user continues to do normal actions and then cleaning the computer. Check our [Datasets page.](category/dataset.html "Dataset")

> NEWS!: We are proud to announce that in the near future Stratosphere will be included inside Snort as a plugin so you will be able to run our Machine Learning detection in your favorite IDS! We need collaborators in this project, so if you love FLOS, security and coding, get in touch with us!").


> NEWS!: We officially announce our Stratosphere Data Analysis project. Our cloud-based implementation of Stratosphere detection free for NGOs and civil defenders. Read more about it [here](category/sda.html "SDA").


---

>
> ### The behavioral-based free software Intrusion Prevention System.
>
The Stratosphere IPS is a free software Intrusion Prevention System that uses Machine Learning to __detect and block__ known malicious behaviors in the network traffic. The behaviors are learnt from highly verified malware and normal traffic connections in our research laboratory. Our goal is to provide the community and specially the NGOs and CSOs with an advanced tool that can protect against targeted attacks.

We believe that **everyone has the right** to protect themselves by using advanced behavioral detection techniques against malware. Since more and more governments and organizations are using this type of technology to **invade the privacy** and security of people, we make public our research in an effort to protect users and raise awareness on the automatic analysis of users behaviors. By using and advancing this tool we may be able to improve our security and to understand the risks of global surveillance.

The core of the Stratosphere IPS is a machine learning algorithm that analyzes individual network connections to extract their behavioral pattern. The patterns of known malicious connections are trained in our laboratory and are then used to detect unknown traffic in new networks. The algorithms were publicly published and the behavioral models are continually being verified in our laboratory. 

The goal of the Stratosphere IPS is to develop a highly advanced and free network-based *malicious actions detector* that can help protect individuals, middle-size organizations, NGOs and almost any type of network. As a free software product we invite the community to verify it and test it. Please see our [download](category/download.html) section.

> ### To protect people by using behavioral algorithms.

The Stratosphere Project consists of four parts: 

1. [Research of behavioral models and detection algorithms.](category/research-blog.html "Research")
    - A continuous improvement of our algorithms.
2. [Development of the Stratosphere IPS.]({filename}../Development/development-stratosphere-ips.md "Development")
    - The Stratosphere IPS to download and use.
3. [Development of the Stratosphere Testing Framework]({filename}../Development/development-stratosphere-testing-framework.md "Stratosphere Testing Framework")
    - The framework used to guarantee the quality of the models.
4. [Datasets of new malware and normal traffic.](category/dataset.html "Dataset")
    - Our datasets to improve and measure our detection performance.

# Free protection for NGOs. Stratosphere Data Analysis Project (SDA)
To protect NGOs for free, we created the Stratosphere Data Analysis Project to provide a cloud-based service where we run our machine learning algorithms to remotely detect the malicious behaviors of infected computers in the NGO's network. Read more [here](category/sda.html "SDA")

# Collaboration
The goal of the Stratosphere IPS Project is to deliver a useful tool to the community, so we would love to get in touch with other researchers that want to collaborate in the project, and with NGOs and CSOs that we can help protect. Please consider sending us an email if you were interested in the project.

# News History
- 2016-05-20: Publication of the alpha version of the Stratosphere Windows IPS! Go and check it out!
- 2016-01-08: We have created a backup of our CTU-13 dataset in case the main repository is down. All the files can be found in [MEGA](https://mega.nz/#F!vdRmBA6D!yMZXx74nnu8GjhdwSF54Sw). Remember that the password for the zip files is _infected_.
- 2015-12-25: We are taking down the dataset site for maintenance. Sorry for the inconvenience.
- 2015-12-18: We moved all the malware files in the dataset to a zip format. The password of all of them is _infected_.
- 2015-12-18: New models of malicious behaviors were added to the alpha version of the Stratosphere Linux IPS.
- Today __2015-12-08__ it is released the first public version of the Stratosphere IPS for Linux operating systems. This is a huge step in the development of the project and we hope to continue improving it. The idea is to test the product in your computers and networks and let us know how it goes. Right now there are only some models to test and more will be added soon.
- __2015-10-12__: As part of our open policy the Stratosphere Project published the original tools used for starting the research on this project. The tools are called __CCDetector__ and __BotnetDetectorsComparer__. They are not officially part of the Stratosphere Project, but they were the foundations used to test the ideas. The tools were used for obtaining the results in the paper ["An empirical comparison of botnet detection methods"](http://www.sciencedirect.com/science/article/pii/S0167404814000923). You can download them and use them to test more ideas. See the blog post [CCDetector and BotnetDetectorComparer](ccdetector-and-botnetdetectorcomparer.html "CCDetector and BotnetDetectorComparer")
- __2015-08-28__: Stratosphere was accepted to give a talk in the Hacktivity Conference! Thanks to the organizers for the opportunity to tell about our project.
- Today July 8th, the Dataset server will be down for maintenance.
- The Stratosphere Project had the opportunity to give a remote talk in the HUSH Hackathon on the [Surveillance and Citizenship Conference](http://www.dcssproject.net/conference/)! Thanks everyone there for the support.
- The Stratosphere Project was honored by being accepted to participate in the CitizenLab Summer Institute 2015 Workshop. Thanks a lot to all the CitizenLab team and the University of Toronto for this opportunity. [More Info](http://citizenlab.org/summerinstitute/2015.html)
- The Stratosphere Project was accepted to give a talk in the Virus Bulletin conference 2015! The called is called "Modelling the network behaviour of malware to block malicious patterns. The Stratosphere Project: a behavioural IPS". Thanks a lot for your support. [More Info](https://www.virusbtn.com/conference/vb2015/abstracts/Garcia.xml)
- The Stratosphere Testing Framework (stf) alpha version was released! Now you can use it to generate the behavioral models of network connections to analyze if their are malicious or not. See the Section [Development of the Stratosphere Testing Framework]({filename}../Development/development-stratosphere-testing-framework.md "Stratosphere Testing Framework"). And also the [Github repository](https://github.com/stratosphereips/StratosphereTestingFramework)
- There is a new blog about how to use the **Stratosphere Testing Framework** to find C&C channels in malware traffic. You can read it [here]({filename}../Research Blog/Example-of-stf-usage.md)
- There is a new blog about the analysis of the traffic of an APK with the **stf** to find C&C channels. See it [here]({filename}../Research Blog/Analysis of the traffic of an APK malware.md)

[Stratosphere Linux IPS]: category/stratospherelinuxips.html
[Stratosphere Windows IPS]: category/stratospherewindowsips.html
