About
#####
:date: 2015-01-21
:url: /category/
:save_as: category/about.html



The Stratosphere IPS project was born in the CTU University of Prague in Czech Republic, as part of
the PhD work of Sebastian García. Currently the project is being funded by the NLnet Foundation from 
Netherlands, to whom we are greatly thankful. Sebastian did his PhD in the ISISTAN Institute of 
UNICEN University in Argentina.

Team
----
The researchers working in the project are:
    - `Sebastian Garcia`_. CTU University, Czech Republic (Send **encrypted** emails to sebastian.garcia@agents.fel.cvut.cz) (`gpg key`_)
    - `Carlos Catania`_. National University of Cuyo, Argentina (harpomaxx@gmail.com)
    - `Maria Jose Erquiaga`_. National University of Cuyo, Argentina (mariajoseerquiaga@gmail.com)
    - Frantisek Střasák. CTU University, Czech Republic (strasfra@fel.cvut.cz)
    - Daniel Šmolík. CTU University, Czech Republic (smolidan@fel.cvut.cz)
    - Yury Kasimov. CTU University, Czech Republic (yury.kasimov@gmail.com)
    - Raúl Benítez. CTU University, Czech Republic (benitrau@fit.cvut.cz)
    - Rashad Suleymanov. Tallinn University of Technology, Estonia (abdullaoglu@gmail.com)
    - Pablo Torres. National University of Cuyo, Argentina (pablo.dtorres@gmail.com)
    - David Kubeša. CTU University, Czech Republic (d.kubesa@gmail.com)

Volunteers & Collaborators
--------------------------
The people collaborating with us are:
    - Veronica Valeros: The Stratosphere Project is proud and thankful to have **Veronica Valeros** (@verovaleros) as a very important pro-bonus collaborator, malware hunter and Threat Analyst. Her insight and work is invaluable to help us complete our goals.

Founding and Support
--------------------
Without the support and guidance of the `NlNet Foundation`_ and the `CTU University`_, this project
would never exist. Please strongly consider a donation to the NLnet Foundation, they are highly
qualified to help improve the security and privacy of the Internet.

The Faculty of Electrical Engineering (`FEL`_) of the CTU University and the Center for Computing and ICT (`SVTI`_) of this Faculty are a very important technical support and collaborators in this project. We deeply appreciate and thanks their effort.

We are specially thankful for the selfless support of our project by `Virus Total`_. They are a very valuable resource for our research.

Contact
-------
For communications with our group about collaborations, issues, questions or bug reports, please send an email to sebastian.garcia@agents.fel.cvut.cz

|NlNetImageLink|_ |CTUImageLink|_ |VirusTotal|_

.. |logo| image:: ../images/logo-1.jpg

.. |NlNetImageLink| image:: ../images/logo-nlnet.gif
    :width: 200px

.. _VirusTotal: https://www.virustotal.com/

.. |VirusTotal| image:: ../images/VirusTotal-logo.png
    :width: 200px


.. _NlNetImageLink: http://www.ntnet.nl/


.. |CTUImageLink| image:: ../images/logo-cvut.png
    :width: 200px
.. _CTUImageLink: http://www.felk.cvut.cz/


.. _`Sebastian Garcia`: https://www.researchgate.net/profile/Sebastian_Garcia6
.. _`Carlos Catania`: https://www.researchgate.net/profile/Carlos_Catania/publications
.. _`Maria Jose Erquiaga`: https://www.researchgate.net/profile/Maria_Erquiaga2
.. _`NlNet Foundation`: https://www.nlnet.nl
.. _`CTU University`: https://www.cvut.cz
.. _`gpg key`: https://pgp.mit.edu/pks/lookup?op=get&search=0x298BCB1E5CB80F49
.. _`FEL`: https://www.fel.cvut.cz
.. _`Virus Total`: https://www.virustotal.com
.. _`SVTI`: https://www.fel.cvut.cz/cz/13373/ 
.. _`Frantisek Střasák`: https://www.cvut.cz/
